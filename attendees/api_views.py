from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Attendee
import json
from events.models import Conference


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        response = []
        attendees = Attendee.objects.all()
        for attendee in attendees:
            response.append(
                {
                    "name": attendee.name,
                    "href": attendee.get_api_url(),
                }
            )
        return JsonResponse({"attendees": response})
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            {
                "message": "Attendee created successfully",
                "id": attendee.id,
                "name": attendee.name,
                "href": attendee.get_api_url(),
            }
        )


def api_show_attendee(request, id):
    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    attendee = Attendee.objects.get(id=id)
    conference = attendee.conference

    return JsonResponse(
        {
            "email": attendee.email,
            "name": attendee.name,
            "company_name": attendee.company_name,
            "created": attendee.created,
            "conference": {
                "name": conference.name,
                "href": conference.get_api_url(),
            },
        }
    )
