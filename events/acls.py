import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_pexels_photo(query):
    url = f"https://api.pexels.com/v1/search?query={query}&per_page=1"
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    data = response.json()
    if data.get("photos"):
        return data["photos"][0]
    return None


def get_weather_data(city):
    url = f"https://api.openweathermap.org/data/2.5/weather?q={city}&appid={OPEN_WEATHER_API_KEY}&units=metric"
    response = requests.get(url)
    data = response.json()
    return data
